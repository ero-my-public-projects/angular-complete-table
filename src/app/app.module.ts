import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WrapperTableComponent } from './component/wrapper-table/wrapper-table.component';
import { ContainerTableExempleComponent } from './exemple/container-table-exemple/container-table-exemple.component';
import { ViewTableExempleComponent } from './exemple/view-table-exemple/view-table-exemple.component';

@NgModule({
  declarations: [
    AppComponent,
    WrapperTableComponent,
    ContainerTableExempleComponent,
    ViewTableExempleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
