export interface Person {
  pseudo: string;
  firstname: string;
  lastname: string;
  age: number;
}
