import {Component, Input, ViewChild, OnChanges, SimpleChanges, AfterViewInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Person} from '../models/model';

@Component({
  selector: 'app-view-table-exemple',
  templateUrl: './view-table-exemple.component.html',
  styleUrls: ['./view-table-exemple.component.css']
})
export class ViewTableExempleComponent implements OnChanges, AfterViewInit {

  @Input() data: Person[];
  @Input() displayedColumns: string[];
  dataSource: MatTableDataSource<Person> = new MatTableDataSource<Person>();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  pageSizeOptions = [5, 10, 15, 20];
  pageSize = 5;

  ngOnChanges(changes: SimpleChanges): void {
    if (Array.isArray(changes?.data?.currentValue)) {
      this.dataSource.data = this.data;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource);
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sortingDataAccessor = (data, sortHeaderId) => {
      let stringSort;
      switch (sortHeaderId) {
        case 'name':
          stringSort = `${data?.firstname ?? ''} ${data?.lastname ?? ''}`;
          break;
        case 'age':
          return data?.age;
          break;
        default:
          stringSort = data[sortHeaderId];
      }
      return stringSort?.trim()?.normalize('NFD').replace(/\p{Diacritic}/gu, '').toLowerCase();
    };
  }

}
