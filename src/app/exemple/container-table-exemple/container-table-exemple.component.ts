import {Component, OnInit} from '@angular/core';
import {Person} from '../models/model';


@Component({
  selector: 'app-container-table-exemple',
  styleUrls: ['container-table-exemple.component.css'],
  templateUrl: 'container-table-exemple.component.html',
})
export class ContainerTableExempleComponent implements OnInit {
  displayedColumns: string[] = ['pseudo', 'name', 'age'];
  data: Person[];

  ngOnInit(): void {
    setTimeout(() => {
      this.data = [
        {pseudo: 'aaron', firstname: 'Mireille', lastname: 'Dupond', age: 3},
        {pseudo: 'chacha', firstname: 'Albert', lastname: 'Marchal', age: 75},
        {pseudo: 'abd', firstname: 'Sophie', lastname: 'Roux', age: null},
        {pseudo: 'dogman', firstname: 'Janette', lastname: 'Rouge', age: 46},
        {pseudo: '1234go', firstname: 'Patrick', lastname: 'Dubois', age: 49},
        {pseudo: 'AAAH', firstname: null, lastname: 'Poulet', age: 3},
        {pseudo: '_36_', firstname: 'Albert', lastname: 'Bucheron', age: 20},
        {pseudo: null, firstname: 'Albert', lastname: null, age: 105},
        {pseudo: 'zaz', firstname: 'Yohan', lastname: 'Philippe', age: 18},
        {pseudo: 'Zak', firstname: null, lastname: null, age: 36},
      ];
    }, 2000);
  }
}
